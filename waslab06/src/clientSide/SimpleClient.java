package clientSide;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.google.appengine.repackaged.org.json.JSONArray;
import com.google.appengine.repackaged.org.json.JSONException;
import com.google.appengine.repackaged.org.json.JSONObject;

public class SimpleClient {


	private static String TWEET_COLLECTION_URI = "http://localhost:8080/waslab06/rest/tweets";

	public static void main(String[] args) {

		ClientConfig config = new ClientConfig();

		Client client = ClientBuilder.newClient(config);

		/*
		 Task#2: Set authorization filter here 
		 */

		WebTarget target = client.target(TWEET_COLLECTION_URI);    

		/* 
		 * Task#2: Post New Tweet Here
		 */



		/*
		 * Task#3: Like tweet added in task#2
		 */


		Response response = target.request(MediaType.APPLICATION_JSON).get();
		System.out.println("=======================================");
		int status = response.getStatus();
		String body = response.readEntity(String.class);
		System.out.println(status);
		if (status == 200) {
			try {
				JSONArray entity = new JSONArray(body);
				for (int i = 0; i < entity.length(); i++) 
					System.out.println(tweetToString(entity.getJSONObject(i)));
			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else 
			System.out.println(body);

		/*
		 *  Task#4: Delete tweet added in task#2
		 */

	}

	private static String tweetToString(JSONObject tweet) {
		String result = "tweet #";
		try {
			result += tweet.get("tweet_id");
			result += " | ";
			result += tweet.get("user_name");
			result += ": ";
			result += tweet.getString("text");
			result += " | ";
			result += tweet.getString("likes");
			result += " likes | ";
			result += tweet.getString("time");
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
}
